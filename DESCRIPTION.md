IMMICH ( /ˈimij/ ) is an open source, high performance backup solution for videos and photos on your mobile phone.

### Features

* Upload and view videos and photos
* Auto backup when the app is opened
* Selective album(s) for backup
* Download photos and videos to local device
* Multi-user support
* Album
* Shared Albums
* Quick navigation with draggable scrollbar
* Support RAW (HEIC, HEIF, DNG, Apple ProRaw)
* Metadata view (EXIF, map)
* Search by metadata, objects and image tags
* Administrative functions (user management)
* Background backup
* Virtual scroll

## Mobile Apps

* [Android](https://play.google.com/store/apps/details?id=app.alextran.immich)
* [iOS](https://apps.apple.com/us/app/immich/id1613945652)
