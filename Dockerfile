FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

# /usr/src/app is where upstream docker puts the app and expects that in code
RUN mkdir -p /app/code /usr/src/app
WORKDIR /app/code

#  from the jellyfin package since we need more current ffmpeg
RUN curl -o /tmp/key.gpg.key https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key && apt-key add /tmp/key.gpg.key && \
    echo 'deb [arch=amd64] https://repo.jellyfin.org/ubuntu jammy main' > /etc/apt/sources.list.d/jellyfin.list

RUN apt-get update && \
    add-apt-repository -y ppa:deadsnakes && \
    apt-get install -y libheif1 libvips42 libvips-dev jellyfin-ffmpeg6 && \
    apt-get install -y --no-install-recommends python3.11 python3.11-dev openjdk-19-jre-headless && \
    rm -r /var/cache/apt /var/lib/apt/lists

RUN ldconfig /usr/lib/jellyfin-ffmpeg/lib
RUN rm /usr/bin/ffmpeg && ln -s /usr/lib/jellyfin-ffmpeg/ffmpeg /usr/bin
RUN rm /usr/bin/ffprobe && ln -s /usr/lib/jellyfin-ffmpeg/ffprobe /usr/bin

# https://github.com/immich-app/base-images/blob/master/server/Dockerfile#L61
RUN mkdir /usr/src/resources/ && \
    curl https://download.geonames.org/export/dump/cities500.zip -o /usr/src/resources/cities500.zip && \
    unzip /usr/src/resources/cities500.zip -d /usr/src/resources/ && \
    rm /usr/src/resources/cities500.zip && date --iso-8601=seconds | tr -d "\n" > /usr/src/resources/geodata-date.txt
RUN curl https://download.geonames.org/export/dump/admin1CodesASCII.txt -o /usr/src/resources/admin1CodesASCII.txt
RUN curl https://download.geonames.org/export/dump/admin2Codes.txt -o /usr/src/resources/admin2Codes.txt

# supervisor
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/ /etc/supervisor/conf.d/

ARG NODE_VERSION=20.15.1
RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

RUN pip install --upgrade pip && pip install poetry
RUN poetry config installer.max-workers 10 && poetry config virtualenvs.create false

RUN virtualenv -p /usr/bin/python3.11 /app/code/venv
ENV PATH=/app/code/venv/bin:$PATH
ENV VIRTUAL_ENV="/app/code/venv"
ENV PYTHONDONTWRITEBYTECODE=1 \
  PYTHONUNBUFFERED=1 \
  PIP_NO_CACHE_DIR=true

# renovate: datasource=github-releases depName=immich-app/immich versioning=semver extractVersion=^v(?<version>.+)$
ARG IMMICH_VERSION=1.126.1

RUN curl -LSs https://github.com/immich-app/immich/archive/refs/tags/v${IMMICH_VERSION}.tar.gz | tar -xz -C /app/code/ --strip-components 1 -f -

# machine learning
# https://github.com/immich-app/immich/blob/main/machine-learning/README.md#setup
RUN cd /app/code/machine-learning && poetry install --sync --no-interaction --no-ansi --no-root --with cpu --without dev

WORKDIR /app/code/open-api
RUN ./bin/generate-open-api.sh

# server
WORKDIR /app/code/server
RUN npm ci && \
    # sharp-linux-x64 and sharp-linux-arm64 are the only ones we need
    # they're marked as optional dependencies, so we need to copy them manually after pruning
    rm -rf node_modules/@img/sharp-libvips* && \
    rm -rf node_modules/@img/sharp-linuxmusl-x64
RUN npm run build

# geodata
# https://github.com/immich-app/base-images/blob/main/server/Dockerfile#L48C1-L55C1
ADD https://download.geonames.org/export/dump/admin1CodesASCII.txt /usr/src/app/geodata/admin1CodesASCII.txt
ADD https://download.geonames.org/export/dump/admin2Codes.txt /usr/src/app/geodata/admin2Codes.txt
ADD https://raw.githubusercontent.com/nvkelso/natural-earth-vector/v5.1.2/geojson/ne_10m_admin_0_countries.geojson /usr/src/app/geodata/ne_10m_admin_0_countries.geojson
RUN wget https://download.geonames.org/export/dump/cities500.zip -O /usr/src/app/geodata/cities500.zip && \
    unzip /usr/src/app/geodata/cities500.zip -d /usr/src/app/geodata/ && \
    rm /usr/src/app/geodata/cities500.zip && date --iso-8601=seconds | tr -d "\n" > /usr/src/app/geodata/geodata-date.txt
RUN chown cloudron:cloudron /usr/src/app/geodata/*

# web
WORKDIR /app/code/web
RUN npm ci
RUN npm run build

# out
WORKDIR /usr/src/app
RUN mv /app/code/machine-learning ./machine-learning
RUN mv /app/code/server/node_modules ./node_modules
RUN mv /app/code/server/dist ./dist
RUN mv /app/code/server/bin ./bin
RUN mv /app/code/server/resources ./resources
RUN mv /app/code/web/build ./www
RUN mv /app/code/server/package.json /app/code/server/package-lock.json ./

RUN ln -s /app/data/upload /usr/src/app/upload

# we have to move the venv first!
# RUN rm -rf /app/code

COPY admin-cli.sh /usr/bin/admin-cli
COPY env.sh start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
