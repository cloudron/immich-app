#!/usr/bin/env node

/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD ) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const EMAIL = 'admin@cloudron.local';
    const PASSWORD = 'changeme';
    const FIRSTNAME = 'Jennifer';
    const LASTNAME = 'Held';

    const OIDC_USERNAME = process.env.USERNAME;
    const OIDC_PASSWORD = process.env.PASSWORD;

    let browser, app, apiSecret, imageId;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();

        console.log('Installing the immich cli...');
        execSync('npm i -g @immich/cli');
    });

    after(function () {
        browser.quit();
    });

    async function saveScreenshot(fileName) {
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${fileName.replaceAll(' ', '_')}-${new Date().getTime()}.png`, screenshotData, 'base64');
    }

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');
        await saveScreenshot(this.currentTest.title);
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function setup() {
        console.log('Waiting for 10 seconds before setup');
        await browser.sleep(10000);

        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//span[text()="Getting Started"]'));
        await browser.findElement(By.xpath('//span[text()="Getting Started"]')).click();
        await waitForElement(By.xpath('//label[contains(., "Admin Email")]/following-sibling::div/input'));
        await browser.findElement(By.xpath('//label[contains(., "Admin Email")]/following-sibling::div/input')).sendKeys(EMAIL);
        await browser.findElement(By.xpath('//label[contains(., "Admin Password")]/following-sibling::div/input')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//label[contains(., "Confirm Admin Password")]/following-sibling::div/input')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//label[contains(., "Name")]/following-sibling::div/input')).sendKeys(`${FIRSTNAME} ${LASTNAME}`);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//button[contains(., "Login")]'));
    }

    async function login() {
        await browser.sleep(2500);
        await browser.get(`https://${app.fqdn}/auth/login`);
        await browser.sleep(2500);

        await waitForElement(By.xpath('//button[contains(., "Login")]'));
        await browser.findElement(By.xpath('//label[contains(., "Email")]/following-sibling::div/input')).sendKeys(EMAIL);
        await browser.findElement(By.xpath('//label[contains(., "Password")]/following-sibling::div/input')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();

        await waitForElement(By.xpath('//button[contains(., "Upload")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//section[@id="dashboard-navbar"]//figure')); // initials are separate text nodes J and H
        await browser.findElement(By.xpath('//section[@id="dashboard-navbar"]//figure')).click();
        await waitForElement(By.xpath('//button[contains(., "Sign Out")]'));
        await browser.findElement(By.xpath('//button[contains(., "Sign Out")]')).click();
        await waitForElement(By.xpath('//button[contains(., "Login")]'));
    }

    async function oidcLogin(session = false) {
        await browser.sleep(2500);
        await browser.get(`https://${app.fqdn}/auth/login`);

        await waitForElement(By.xpath('//button[contains(., "Login with")]'));
        await browser.findElement(By.xpath('//button[contains(., "Login with")]')).click();

        if (!session) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(OIDC_USERNAME);
            await browser.findElement(By.id('inputPassword')).sendKeys(OIDC_PASSWORD);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//button[contains(., "Upload")]'));
    }

    async function oidcLogout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//section[@id="dashboard-navbar"]//figure')); // initials are separate text nodes J and H
        await browser.findElement(By.xpath('//section[@id="dashboard-navbar"]//figure')).click();
        await waitForElement(By.xpath('//button[contains(., "Sign Out")]'));
        await browser.findElement(By.xpath('//button[contains(., "Sign Out")]')).click();
        await waitForElement(By.xpath('//button[contains(., "Login")]'));
    }

    async function uploadPhoto() {
        await browser.get('about:blank');

        const accessToken = await browser.manage().getCookie('immich_access_token').value;
        const agent = superagent.agent();

        console.log('Login with superagent, for session');
        let response = await agent.post(`https://${app.fqdn}/api/auth/login`).send({ email: EMAIL, password: PASSWORD });
        expect(response.status).to.equal(201);

        console.log(`Upload photo: ${path.resolve('../screenshots/immich-2.jpeg')}`);
        try {
            response = await agent.post(`https://${app.fqdn}/api/assets`)
                .field('deviceAssetId', 'web-immich-2.jpeg-' + parseInt(9999*Math.random()))
                .field('assetType', 'IMAGE')
                .field('fileCreatedAt', new Date().toISOString())
                .field('fileModifiedAt', new Date().toISOString())
                .field('isFavorite', 'false')
                .field('deviceId', 'WEB')
                .field('fileExtension', '.jpeg')
                .field('duration', '0:00:00.000000')
                .attach('assetData', path.resolve('../screenshots/immich-2.jpeg'));
            expect(response.status === 200 || response.status === 201);
        } catch (e) {
            throw e
        }

        imageId = response.body.id;

        console.log('Uploading successful. Image ID:', imageId);

        // give immich plenty of time to process the job
        await browser.sleep(10000);
    }

    async function photoExists() {
        await browser.get(`https://${app.fqdn}/photos/${imageId}`);
        await browser.sleep(2000);
        await waitForElement(By.xpath('//button[@id="context-menu-button-id-3"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can setup admin', setup);
    it('can login', login);
    it('can upload photo', uploadPhoto);
    it('photo exists', photoExists);
    it('can logout', logout);

    it('can oidc login', oidcLogin);
    it('can oidc logout', oidcLogout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('photo exists', photoExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        browser.sleep(5000);
    });

    it('can login', login);
    it('photo exists', photoExists);
    it('can logout', logout);

    it('can oidc login', oidcLogin.bind(null, true));
    it('can oidc logout', oidcLogout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        browser.sleep(10000);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('photo exists', photoExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id app.immich.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can setup admin', setup);
    it('can login', login);
    it('can upload photo', uploadPhoto);
    it('photo exists', photoExists);
    it('can logout', logout);

    // it('can oidc login', oidcLogin.bind(null, true));
    // it('can oidc logout', oidcLogout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); browser.sleep(10000); });

    it('can login', login);
    it('photo exists', photoExists);
    it('can logout', logout);

    it('can oidc login', oidcLogin.bind(null, true));
    it('can oidc logout', oidcLogout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
