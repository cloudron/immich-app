#!/bin/bash

set -eu

echo "=> App startup"

psql_cli="psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} "

echo "=> Ensure directories"
mkdir -p /app/data/upload /app/data/typesense /tmp/reverse-geocoding-dump /tmp/matplotlib
chown -R cloudron:cloudron /app/data /tmp/reverse-geocoding-dump /tmp/matplotlib

# https://immich.app/docs/install/config-file
config_file="/app/data/immich.json"
[[ ! -f $config_file ]] && echo "{}" > $config_file

echo "=> Setup environment"
source /app/pkg/env.sh

echo "=> Disable update checker"
cat $config_file | \
    jq '.backup.database.enabled=false' | \
    jq '.newVersionCheck.enabled=false' | \
    jq '.server.externalDomain=env.CLOUDRON_APP_ORIGIN' \
    > ${IMMICH_CONFIG_FILE}

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "=> Setup OpenID Connect"
    cat ${IMMICH_CONFIG_FILE} | \
        jq '.oauth.enabled=true' | \
        jq '.oauth.issuerUrl=env.CLOUDRON_OIDC_ISSUER' | \
        jq '.oauth.clientId=env.CLOUDRON_OIDC_CLIENT_ID' | \
        jq '.oauth.clientSecret=env.CLOUDRON_OIDC_CLIENT_SECRET' | \
        jq '.oauth.scope="openid email profile"' | \
        jq '.oauth.signingAlgorithm="RS256"' | \
        jq '.oauth.storageLabelClaim="preferred_username"' | \
        jq '.oauth.storageQuotaClaim="immich_quota"' | \
        jq '.oauth.defaultStorageQuota=0' | \
        jq ".oauth.buttonText=\"Login with ${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}\"" | \
        jq '.oauth.autoRegister=true' | \
        jq '.oauth.autoLaunch=false' | \
        jq '.oauth.mobileOverrideEnabled=false' | \
        jq '.oauth.mobileRedirectUri=""' \
        > /run/immich.json.tmp && mv /run/immich.json.tmp ${IMMICH_CONFIG_FILE}
fi

if [[ -n "${CLOUDRON_MAIL_SMTP_SERVER:-}" ]]; then
    echo "=> Setup Notification Email"
    export MAIL_FROM="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Immich} <${CLOUDRON_MAIL_FROM}>"
    cat ${IMMICH_CONFIG_FILE} | \
        jq '.notifications.smtp.enabled=true' | \
        jq '.notifications.smtp.from=env.MAIL_FROM' | \
        jq '.notifications.smtp.replyTo=env.MAIL_FROM' | \
        jq '.notifications.smtp.transport.host=env.CLOUDRON_MAIL_SMTP_SERVER' | \
        jq '.notifications.smtp.transport.port=2525' | \
        jq '.notifications.smtp.transport.username=env.CLOUDRON_MAIL_SMTP_USERNAME' | \
        jq '.notifications.smtp.transport.password=env.CLOUDRON_MAIL_SMTP_PASSWORD' | \
        jq '.notifications.smtp.transport.ignoreCert=true' \
        > /run/immich.json.tmp && mv /run/immich.json.tmp ${IMMICH_CONFIG_FILE}
fi

echo "=> Prepare database for vectors extension"
curl -v -X POST "http://${CLOUDRON_POSTGRESQL_HOST}:3000/init-or-upgrade-vectors?database=${CLOUDRON_POSTGRESQL_DATABASE}&username=${CLOUDRON_POSTGRESQL_USERNAME}"

echo "=> Start supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i immich
