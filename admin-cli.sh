#!/bin/bash

source /app/pkg/env.sh

gosu cloudron:cloudron node /app/code/server/dist/main admin-cli "$@"
