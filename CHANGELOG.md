[0.1.0]
* Initial version

[0.2.0]
* Update version to 1.24.0_34
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.24.0_34-dev)
* Prepare for tensorflow machine learning support (currently disabled)
* Fixed - Delete Webp thumbnail upon asset deletion @panoti in #489
* Feature: Added bull queue prefix by @panoti in #490

[0.3.0]
* Update version to 1.25.0_35
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.25.0_35-dev)
* Fixed - Removed albumThumbnailAssetId when the album is empty by @panoti in https://github.com/immich-app/immich/pull/495
* Feature - Added x-adobe-dng to supported mime type by @alextran1502 in https://github.com/immich-app/immich/pull/504
* Feature - Preserve caption fields and extract MediaInfo for video by @panoti in https://github.com/immich-app/immich/pull/505
* Optimization - Minimize containers' size by @panoti in https://github.com/immich-app/immich/pull/506

[0.4.0]
* Add machine-learning with tensorflow

[0.5.0]
* Update Immich to 1.26.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.26.0_36-dev)
* Feature - Support tiff uploading by @panoti in #513
* Refactor - move constants into the shared library by @panoti in #522
* Fixed - Large file size cannot be saved to EXIF table due to incorrect data type (integer vs bigint) in the Exif table by @alextran1502 in #534
* Feature - Video player is buffered for faster startup time and reduces network usage by @panoti in #520
* Fixed - Skipping assets when navigating with the keyboard by @alextran1502 in #531
* Migrate SvelteKit to the latest version 431 by @alextran1502 in #526
* Fixed - Show the first two letter of user first and last name when profile image not existed by @alextran1502 in #532
* Feature - Implemented notification box for web by @alextran1502 in #533
* Fixed - Limit number of file upload on web by @alextran1502 in #535
* Optimization - Added error handling notification by @alextran1502 in #536

[0.6.0]
* Update Immich to 1.27.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.27.0_37-dev)
* optimization(server): add asset repository and refactor asset service by @alextran1502 in #540
* fix(server): avoid loading all file content on memory by @panoti in #545
* fix(server): parse all image formats and enrich metadata by @panoti in #547
* feat(server): calculate sha1 checksum by @panoti in #525
* feat(server): generate a checksum for previously uploaded assets by @panoti in #558
* feat(server): support 3gpp format by @panoti in #582
* feat(server): de-duplication by @panoti in #557
* hotfix(server): skip EXIF extraction on duplicate file by @panoti in #590
* fix(server): change the createdAt and modifiedAt to the correct type in the database by @alextran1502 in #591

[0.7.0]
* Update Immich to 1.28.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.28.0_38-dev)
* Update README.md by @godismyjudge95 in #599
* fix(mobile): remove empty translations by @bo0tzz in #620
* fix(mobile): search page crashes the app on some Android models by @alextran1502 in #610
* fix(web): add scroll bar into detail panel by @panoti in #615
* feat(web): add asset and album count info by @alextran1502 in #623
* feat(web): add web test setup by @jbaez in #597
* fix(web): stop showing version announcement on the first run of a new web instance by @alextran1502 in #609

[0.8.0]
* Update Immich to 1.28.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.28.1_39-dev)
* feat(web) add all server checks to CI - fix lint issues by @jbaez in https://github.com/immich-app/immich/pull/633
* feat(web) add web UI components tests setup by @jbaez in https://github.com/immich-app/immich/pull/612
* feat(web) add web test / check commands and workflow to run in CI by @jbaez in https://github.com/immich-app/immich/pull/642
* fix(web) fix Notification components possible memory leaks by @jbaez in https://github.com/immich-app/immich/pull/650
* feat(web) add scrollbar with timeline information by @alextran1502 in https://github.com/immich-app/immich/pull/658

[0.8.1]
* Update Immich to 1.28.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.28.2_40-dev)
* fix(mobile): Background backup not running in release mode by @alextran1502 in https://github.com/immich-app/immich/pull/664
* fix(server): loop on checksum generation by @panoti in https://github.com/immich-app/immich/pull/662

[0.8.2]
* Update Immich to 1.28.3
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.28.3_40-dev)
* fix(web) incorrect shared album count by @alextran1502 in https://github.com/immich-app/immich/pull/677

[0.8.3]
* Update Immich to 1.28.4
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.28.4_41-dev)
* Added Log level to background service (#685)
* fix(web): datetime display and add TZ into environment by @panoti in #618
* fix(server): remove album thumbnail when the asset is deleted from the database by @alextran1502 in #681
* fix(server): harden inserting process, healing datetime info to insert to database by @alextran1502 in #682
* feat(server): add additional logging level by @alextran1502 in #685

[0.9.0]
* Update Immich to 1.29.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.29.0_42-dev)
* fix(server): mismatch createdAt value in exif table and asset table by @alextran1502 in #688

[0.9.1]
* Update Immich to 1.29.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.29.2_43-dev)

[0.9.2]
* Update Immich to 1.29.4
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.29.4_44-dev)
* Fix error with the API to get asset on the device returning only image, photo is omtted.
* fix(server): query only image when get asset on device by @alextran1502 in https://github.com/immich-app/immich/pull/724

[0.9.3]
* Update Immich to 1.29.5
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.29.5_44-dev)
* Only run scheduled geocoding task once per day by @bo0tzz in #730

[1.0.0]
* Update Immich to 1.30.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.30.0_46-dev)
* fix(server,mobile): Incorrectly record/display timestamp and time zone of the asset by @alextran1502 in #706
* feat(server) Remove mapbox and use local reverse geocoding by @zackpollard in #738
* fix(server): handle missing reverse geocoding admin zones by @zackpollard in #742
* fix(web) navigating forward button get in the way of video control bar by @alextran1502 in #744
* fix(machine-learning) Remove unsused database config by @alextran1502 in #745
* feat(server): support .NEF file by @alextran1502 in #746
* feat(server): missing exif extract nightly task by @zackpollard in #754
* feat(server): Provide a sensible dumpDirectory for the local-reverse-geocoder module by @nebulade in #759

[1.0.1]
* Update Immich to 1.30.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.30.2_48-dev)
* chore(server): Add docker volumes to services by @PixelJonas in https://github.com/immich-app/immich/pull/766
* fix(server): Fix resized thumbnail path not parse correctly by @deepesh16b in https://github.com/immich-app/immich/pull/780

[1.1.0]
* Update Immich to 1.31.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.31.0_49-dev)
* New background jobs UI
* fix(server): Use boolean comparison for DISABLE_REVERSE_GEOCODING config by @bo0tzz in #787
* feat(server/web): Add manual job trigger mechanism to the web by @alextran1502 in #767

[1.1.1]
* Update Immich to 1.31.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.31.1_49-dev)
* fix(server): Delete encoded video when deleting file by @alextran1502 in https://github.com/immich-app/immich/pull/794
* fix(server): Update local-reverse-geocoder to 0.12.5 by @bo0tzz in https://github.com/immich-app/immich/pull/793
* fix(web): Fix z-index ordering of Account Info Box and date-sidebar on web by @AnTheMaker in https://github.com/immich-app/immich/pull/799

[1.2.0]
* Update Immich to 1.32.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.32.0_50-dev)
* chore(repo): Typo/minor cosmetics in README.md by @EvilOlaf in https://github.com/immich-app/immich/pull/801
* feat(server): Remove default JWT_SECRET value in .env by @bo0tzz in https://github.com/immich-app/immich/pull/810
* chore(deps): bump docker/setup-buildx-action from 2.0.0 to 2.1.0 by @dependabot in https://github.com/immich-app/immich/pull/815
* chore(deps): bump docker/build-push-action from 3.1.1 to 3.2.0 by @dependabot in https://github.com/immich-app/immich/pull/816
* chore(deps): bump docker/setup-qemu-action from 2.0.0 to 2.1.0 by @dependabot in https://github.com/immich-app/immich/pull/820
* feat(server): Log a warning if JWT_SECRET key does not have enough bits by @bo0tzz in https://github.com/immich-app/immich/pull/821

[1.2.1]
* Update Immich to 1.32.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.32.1_51-dev)

[1.2.2]
* Update Immich to 1.32.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.33.0_52-dev)
* Dark mode for web
* Server Statistic

[1.2.3]
* Update Immich to 1.33.3
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.33.1_52-dev)
* chore(server) revert Dockerfile by @alextran1502 in #878
* feat(app) add documentation site template by @alextran1502 in #879
* refactor(server): merge auth guards to authentication guard (#877) by @jrasm91 in #877
* feat(server) Extend PUT /album/:id/assets endpoint (#857) by @matthinc in #857
* fix(web) prevent create multiple user when the instance is lagging by @alextran1502 in #882

[1.2.4]
* Update Immich to 1.34.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.34.0_53-dev)
* fix(web) getting asset with available thumbnail when getting asset count by time bucket by @alextran1502 in #900
* feat(web): add a delete button to asset viewer by @anbraten in #896
* feat(server/web): download entire album as zip archive by @zoodyy in #897
* feat(web): add mechanism to add current view asset to an album by @jrasm91 in #923
* chore(web): Modified styling for add to album panel by @alextran1502 in #924
* fix(web): album download progress bar by @jrasm91 in #925
* feat(web): add selected asset on the main timeline to album from selection by @jrasm91 in #926
* fix(server): download album error handling by @jrasm91 in #917
* test(server): add more the tests by @jrasm91 in #911
* fix(server): harden auto pick album thumbnails (#918) by @jrasm91 in #918
* feat(server): reset admin password using CLI command in the server container by @jrasm91 in #928

[1.2.5]
* Update Immich to 1.35.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.35.0_54-dev)
* feat(server, web): Delete and restore user from the admin portal by @zkhan93 in #935
* fix(server): increase JSON body payload limit by @jrasm91 in #941
* feat(web): favorite an asset by @jrasm91 in #939

[1.3.0]
* Update Immich to 1.36.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.36.1_55-dev)
* fix(server): Admin user not created by @alextran1502 in https://github.com/immich-app/immich/pull/996
* fix(web): broken unit tests by @jrasm91 in #947
* build(server): use github-action cache by @jrasm91 in #949
* build(server): refactor multistage builds by @jrasm91 in #955
* feat(server,web): OIDC Implementation by @jrasm91 in #884
* feat(server,web): system config for admin by @zackpollard and @jrasm91 in #959
* feat(server): multi archive downloads by @jrasm91 in #956
* Update Korean translation with the latest version. by @hismethod in #971
* chore(web,mobile): update github repo url by @jrasm91 in #974
* Web: Disallow all robots by @Mortein in #977
* chore(server) refactor serveFile and downloadFile endpoint by @alextran1502 in #978
* Add navbar button to copy image by @bo0tzz in #961
* web(feat): Add support for actions when clicking notifications by @bo0tzz in #966
* feat: support iOS LivePhoto backup by @alextran1502 in #950
* fix(server): Server freezes when getting statistic by @alextran1502 in #994

[1.3.1]
* Update Immich to 1.36.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.36.2_56-dev)
* fix(server): Deleted shared users cause a problem with album retrieval and creation by @alextran1502 in https://github.com/immich-app/immich/pull/1002

[1.3.2]
* Update Immich to 1.37.0
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.37.0_58-dev)
* feat(server,web,mobile): Use binary prefixes for data sizes by @Mortein in #1009
* Fix(web) navbar color overlap and scroll bar incorrect z index by @denck007 in #1018
* fix(server): Prevent delete admin user by @alextran1502 in #1023
* feat(server,web,mobile): activate ETags for all API endpoints and asset serving by @zoodyy in #1031

[1.3.3]
* Update Immich to 1.38.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.38.2_60-dev)
* fix(server): Fix error not able to see asset added by shared user in shared album by @matthinc in https://github.com/immich-app/immich/pull/1094
* feat(server): link via profile.sub by @jrasm91 in #1055
* chore(): add Chinese README file by @chen3stones in #1058
* feat(web): Localize dates and numbers by @Mortein in #1056
* feat(server): Per user asset access control by @matthinc in #993
* fix(web) fix test by @alextran1502 in #1059
* feat(server) Tagging system by @alextran1502 in #1046
* fix(server) fix correct MIME type for Nikon NEF by @alextran1502 in #1060
* fix(server) added TagResponseDto for TagController by @alextran1502 in #1065
* chore(web) Update SvelteKit by @alextran1502 in #1066
* refactor(server): server version logging by @jrasm91 in #1073
* refactor(server): device info service by @jrasm91 in #1071
* feat(server,web): migrate oauth settings from env to system config by @jrasm91 in #1061
* fix(server): link 'immich' by @jrasm91 in #1080
* fix(server): require local admin account by @jrasm91 in #1070
* docs: server commands by @jrasm91 in #1079
* fix(server): unique email database constraint by @jrasm91 in #1082

[1.3.4]
* Fix machine learning component

[1.3.5]
* Update Immich to 1.39.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.39.0_61-dev)
* chore() Fix typo and update notification wording by @HcNguyen111 in #1100
* feat(server) user-defined storage structure by @alextran1502 in #1098
* chore(web) update SvelteKit to 1.0.0 by @alextran1502 in #1110
* feat(web) add user setting page by @alextran1502 in #1115
* chore(server) Add job for storage migration by @alextran1502 in #1117

[1.4.0]
* Update Immich to 1.40.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.40.0_63-dev)
* feat(web,server): link/unlink OAuth account by @jrasm91 in #1154
* feat(web): user profile by @jrasm91 in #1148
* feat(web) using template filename for downloaded file by @alextran1502 in #1164
* feat(server): extend JWTexpiration by @jrasm91 in #1187
* chore(web): linting by @jrasm91 in #1152
* chore(server): increase OAuth timeout by @jrasm91 in #1155

[1.4.1]
* Update Immich to 1.40.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.40.1_63-dev)
* fix(server) Cannot change first-time password due to null value in first and last name payload from the mobile app by @alextran1502 in https://github.com/immich-app/immich/pull/1205
* feat(server,web): update email address by @jrasm91 in https://github.com/immich-app/immich/pull/1186
* chore(server): fix unit test by @jrasm91 in https://github.com/immich-app/immich/pull/1194
* Fixed translations and added missing by @DVerdeV in https://github.com/immich-app/immich/pull/1201

[1.5.0]
* Update Immich to 1.41.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.41.0_64-dev)
* Highlight - Public sharing
* fix(nginx): define stopsignal by @otbutz in #1207
* feat(server): mobile oauth with custom scheme redirect uri by @jrasm91 in #1204
* fix(docker-build): start main process with exec by @bt90 in #1210
* chore(web): show corresponding ffmpeg argument by @jrasm91 in #1217

[1.5.1]
* Update Immich to 1.41.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.41.1_64-dev)
* fix(web) show EXIF info in public shared by @alextran1502 in https://github.com/immich-app/immich/pull/1283
* fix(Nginx): Switch to sh by @otbutz in https://github.com/immich-app/immich/pull/1282
* fix(Nginx): fix entry point by @otbutz in https://github.com/immich-app/immich/pull/1284
* fix(web) add disable property to generated shared link by @alextran1502 in https://github.com/immich-app/immich/pull/1287
* fix(server) fix order of assets in the shared album to be the same as the actual album by @alextran1502 in https://github.com/immich-app/immich/pull/1293

[1.6.0]
* Update Immich to 1.42.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.42.0_65-dev)
* Individual assets public share
* Fix issue with incorrect time due to unknown timezone in EXIF data
* feat(web): meta tags for share links by @jrasm91 in #1290
* feat(web): Individual assets shared mechanism by @alextran1502 in #1317
* chore(web): clamp modal width to viewport by @jrasm91 in #1297

[1.7.0]
* Update Immich to 1.43.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.43.1)
* Auto-discovery URL on the mobile app
* Change of the token authentication mechanism
* Additional options to public shared link
* Jobs can now be rerun on all assets
* Add a favorite page on the web
* Add an additional option for video transcoding, now it is smarter. Any video that is not under the desired codec (h264 as default) will be transcoded.
* File's hash is now computed on uploaded, this fixes the issue with timing out of the request.
* Further clean up the code base for more advanced features.
* We have improved the code base by adding more tests, and additional CI/CD pipelines that will ensure the quality of the application in the future in each release and reduce the number of regression bugs.
* Incorrect validation return value skipping API checking for CLI
* Incorrect custom port value used in microservices

[1.7.1]
* Update Immich to 1.44.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.44.0)
* feat(web): More localisation by @Mortein in #1441
* fix(web) long album name break styling for shared album card by @alextran1502 in #1473
* refactor(server): move asset upload job to domain by @jrasm91 in #1434
* refactor(server): asset service - upload asset by @jrasm91 in #1438
* refactor(server): device info by @jrasm91 in #1490
* refactor(server): auth guard by @jrasm91 in #1472
* fix(server): re-enable Redis unix socket support by @zoodyy in #1494
* fix(server): Cannot remove album with shared links by @alextran1502 in #1479
* chore(server): cookie changes to SameSite=Lax by @samip5 in #1467

[1.7.2]
* Update Immich to 1.45.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.45.0)

[1.8.0]
* Update Immich to 1.46.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.46.0)
* **Breaking Change:** The server and mobile app will both need to be on version v1.46.0 to work correctly due to some API signature changes between the server and the clients.
* Favorite view on the mobile app
* Dynamic layout on the mobile app
* Improving the detail view on the mobile app

[1.8.1]
* Update Immich to 1.46.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.46.1)
* fix(ci): Add missing checkout step to prepare-release workflow by @bo0tzz in #1709
* fix(server): #1715 fk constraint violation when updating to 1.46 with deleted users and albums by @zackpollard in #1716

[1.9.0]
* Update Immich to 1.47.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.47.2)
* fix(web): Lodash issue in Svelte by @alextran1502 in #1749
* fix(web): es-module import error

[1.9.1]
* Update Immich to 1.47.3
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.47.3)
* #1753 Bad Gateway due to oversize header
* #1757 Uploading photos through album public link does not show in album
* #1703 Dynamically Support IPv6

[1.10.0]
* Update Immich to 1.48.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.48.1)
* The server and the mobile app must be on the same version to function correctly.
* hotfix(server): Object detection query and get server stats by @alextran1502 in https://github.com/immich-app/immich/pull/1823
* hotfix(server): getAlbumByAssetId alters album content by @alextran1502 in https://github.com/immich-app/immich/pull/1828

[1.11.0]
* Update Immich to 1.49.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.49.0)
* fix(server): album repo unused variable by @alextran1502 in #1831
* fix(server): Flask not found by @alextran1502 in #1830
* fix(web): hide img alt text while loading by @michelheusschen in #1834
* feat(web): theme/locale preferences and improve SSR by @michelheusschen in #1832
* refactor(web): combine api and serverApi by @michelheusschen in #1833
* fix(machine-learning): Add the command to execute at startup by @samip5 in #1843

[1.12.0]
* Update Immich to 1.50.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.50.1)
* fix(web): sharing of access token in server API by @michelheusschen in #1858
* feat(web): improved server stats by @michelheusschen in #1870
* fix(web): don't log unauthorized errors by @michelheusschen in #1871
* chore(web): update dependencies by @michelheusschen in #1877
* fix(web): use correct api for asset share page by @michelheusschen in #1879
* fix(web): setInterval outside onMount by @michelheusschen in #1883
* fix(web): layout nesting by @michelheusschen in #1881
* feat(web): re-add version announcement by @michelheusschen in #1887
* feat(server): improve API specification by @michelheusschen in #1853
* refactor(server): jobs and processors by @jrasm91 in #1787
* chore(server): update package-lock.json version by @alextran1502 in #1884
* feat(server) Enable ML support for ARM CPUs by @ollywelch in #1880
* fix(server) long album load time on Album and Sharing page by @alextran1502 in #1890
* fix(server): machine learning only take results with > 90% confidence by @zackpollard in #1875

[1.13.0]
* Update Immich to 1.51.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.51.0)
* Integrating Typesense and CLIP for a better search system
* Allow the mobile app to work offline without an internet connection or when the server is down.

[1.13.1]
* Update Immich to 1.51.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.51.1)
* fix(server): search and explore issues by @jrasm91 in #2029
* fix(server): search and explore part 2 by @jrasm91 in #2031

[1.13.2]
* Update Immich to 1.51.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.51.2)
* feat(web): show current/saved template in preset dropdown by @jrasm91 in #2040
* chore(setup): always restart typesense by @jrasm91 in #2042
* refactor(server): server-info by @jrasm91 in #2038
* fix(server): update asset alias on new install by @jrasm91 in #2044

[1.14.0]
* Update Immich to 1.52.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.52.0)
* feat(server): Allow .mkv, .wmv, .flv, .mpg videos to be uploaded by @samip5 in #2045
* fix(server): Do not change file ext case on upload by @bo0tzz in #2056
* fix(server): invalid video duration format by @michelheusschen in #2058
* refactor(server): asset serve files by @jrasm91 in #2052
* refactor(server): media service by @jrasm91 in #2051
* chore(server): cleanup controllers by @jrasm91 in #2065
* refactor(server): common by @jrasm91 in #2066
* refactor(server): cron jobs by @jrasm91 in #2067
* feat(server): split generated content into a separate folder by @jrasm91 in #2047
* feat(server): improve and refactor get all albums by @michelheusschen in #2048
* fix(server): get all query does not respect asset type by @alextran1502 in #2089
* feat(server): require auth for more endpoints by @michelheusschen in #2092
* fix(server): increase Typesense start-up settings by @jrasm91 in #2095
* feat(server): change clip embedding entity type by @michelheusschen in #2091
* feat(server): resume queues by @jrasm91 in #2104
* feat(server): add transcode presets by @brighteyed in #2084
* chore(server): cleanup template variables by @jrasm91 in #2107
* feat(server): apply storage migration after exif completes by @jrasm91 in #2093
* feat(web): better search bar by @alextran1502 in #2062
* fix(web): remove protocol header by @michelheusschen in #2068
* feat(web): improve and refactor thumbnails by @michelheusschen in #2087

[1.14.1]
* Update Immich to 1.52.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.52.1)
* fix(server): add paused property to JobCountsDto
* fix(server): incorrect video file path to serve for mobile vs web

[1.15.0]
* Update Immich to 1.53.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.53.0)
* feat(mobile): improved logging page experience by @alextran1502 in #2158
* feat(server): add faststart to ffmpeg options by @michelheusschen in #2138
* feat(server): apply ValidationPipe on controllers by @michelheusschen in #2137
* feat(server): redis sentinel support by @samip5 in #2141
* feat(server): improve validation in controllers by @michelheusschen in #2149
* feat(server): Support TypeSense High-Availibility configuration by @samip5 in #2146
* feat(server): enhanced thumbnails generation code by @brighteyed in #2147
* feat(server): ffmpeg quality options improvements by @zackpollard in #2161
* feat(server): transcoding improvements by @zackpollard in #2171
* feat(server): add timezone to exif entity by @AndreAle94 in #1894

[1.15.1]
* Update Immich to 1.54.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.54.0)
* feat(web): allow admin to update email of users by @michelheusschen in #2189
* fix(web): search page navigation and asset select by @michelheusschen in #2191
* refactor(web): centralize buttons by @michelheusschen in #2200
* feat(web): add justify layout for GalleryViewer by @alextran1502 in #2207
* fix(web): show OAuth login button when disabled by @alextran1502 in #2219
* feat(server/web): Implement Archive by @brighteyed in #2225
* feat(web): More responsive web frontend by @faupau03 in #2245
* feat(web): Allow closing modals with Escape key by @bo0tzz in #2257
* fix(web): fix search 400 error when only entering m: by @faupau03 in #2261
* feat(web): small responsivness improvements regarding mobile use by @faupau03 in #2255
* fix(web): show noscript message when js not enabled by @faupau03 in #2274
* fix(web): accountinfopanel not closing on button press by @faupau03 in #2276
* feat(web): smaller thumbnail in explore so at least 2 photos are in a row by @faupau03 in #2277
* fix(web): noscript message by @michelheusschen in #2278
* fix(web): empty album is not auto deleted by @alextran1502 in #2283
* feat(server): improve validation of albums by @michelheusschen in #2188
* chore(server): redis error handling by @samip5 in #2212
* fix(server): expand tests and add avi, mov to mime types by @samip5 in #2213
* chore(server): remove unneeded debug logging by @samip5 in #2203
* chore(server): update openapi dependencies by @michelheusschen in #2205
* fix(server): cannot delete an asset if presented in album(s) by @alextran1502 in #2223
* feat(server): add originalFileName to asset table by @alextran1502 in #2231
* chore(server): better logging for error messages by @alextran1502 in #2230
* fix(server): jobs using stale path by @alextran1502 in #2233
* fix(server): generate thumbnail job uses stale path by @alextran1502 in #2236
* feat(server/web/mobile): Add description mechanism by @alextran1502 in #2237

[1.15.2]
* Update Immich to 1.54.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.54.1)
* Add a button to archive and unarchive an asset in the detail view.
* Add action buttons to the search result page.
* Fixed justified layout in the gallery view.
* Show a smaller thumbnail size on the mobile view of the web.

[1.16.0]
* Update Immich to 1.55.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.55.0)
* fix(web): correct color sidebar button when selected in dark mode by @alextran1502 in #2322
* feat(web): PWA integration add apple icons and manifest file by @faupau03 in #2310
* feat(web, server): manage authorized devices by @jrasm91 in #2329
* fix(web): asset viewer navbar overlapping with details tab and context menu not closing on button press (except in album viewer) by @faupau03 in #2323
* chore(web): organize user settings by @jrasm91 in #2340
* feat(web): Global map showing all assets with geo information by @matthinc in #2355
* fix(web): timeline distortion when scrolling due to rerender of scrollbar bucket and thumbnail size by @alextran1502 in #2398
* feat(web): improve map styling and interaction by @alextran1502 in #2399
* fix(server): use current schema for search/explore by @jrasm91 in #2331
* fix(server): oauth mobile callback url by @jrasm91 in #2339
* chore(server,mobile): remove device info entity by @jrasm91 in #1527
* ci(server): simplify server npm steps by @jrasm91 in #2352
* feat(server): add api key to openapi spec by @michelheusschen in #2362
* fix(server): pump local-reverse-geocoder to 0.15.2 / Fix Corrupted Reverse Geocoding CSV File by @sakowicz in #2396
* chore(server): standardize process method names by @jrasm91 in #2363

[1.16.1]
* Add missing machine-learning dependencies
* Fix geocoding

[1.16.2]
* Update Immich to 1.55.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.55.0)
* Revert "Bump local-reverse-geocoder to 0.15.2 / Fix Corrupted Reverse Geocoding CSV File" since it causes an issue with K8S deployment.

[1.17.0]
* Add OpenID Connect support for new installations

[1.18.0]
* Update Immich to 1.56.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.56.0)
* Not showing archive items when viewing a person's assets
* Only return 1000 assets for each face to avoid crashing the browser - optimization coming soon.
* Facial Recognition
* Partner Sharing
* Mobile App Lazy Loading
* Logout all devices with a single click

[1.18.1]
* Update Immich to 1.56.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.56.2)
* Load machine learning models on startup

[1.19.0]
* Update Immich to 1.57.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.57.0)
* Map improvements for large galleries
* Introduce a custom storage label for each user account
* Improve video transcoding with new options

[1.19.1]
* Update Immich to 1.57.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.57.1)
* fix(web): loading leaflet in production builds by @michelheusschen in #2526
* fix(web): small fixes for album selection modal by @michelheusschen in #2527

[1.20.0]
* Update Immich to 1.58.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.58.0)
* Use node 18.16.0 to fix fs.promise usage
* Add missing machine-learning dependencies
* The server and the mobile app must be on the same version (v1.58.0) for the Partner sharing feature on the mobile app to work correctly.
* Map filtering
* You can now view your Partner’s assets on the mobile app and also manage your Partner’s accessibility to your timeline on the mobile app.
* Immich’s server can now understand XMP Sidecars files.
* We added support for client-side hashing in preparation for the improved upload process across the clients.
* We improved the stability of the server when handling a large amount of new incoming assets by managing the queues better.

[1.21.0]
* Update Immich to 1.59.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.59.0)
* Fixed issue with cannot access the Explore page due to mismatch in Typesense data
* The mobile app and the server has to be on the same version (v1.59.0) for the app to work correctly.
* In this release, we added scroll to zoom ability to the asset viewer, made minor improvements to the web UI, and fixed some bugs we introduced in the last release.

[1.22.0]
* Update Immich to 1.60.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.60.0)
* Added mechanism to dynamically change the job concurrency
* Remove the object detection mechanism since the results are poor and not very useful at the moment. However, we are still keeping the Image Classification.

[1.23.0]
* Update Immich to 1.61.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.61.0)
* Client-side hashing on the mobile app
* Justified layout for timeline view on the web
* Proper support for RAW file formats
* Memories feature on the web
* Select-all button on all web views

[1.24.0]
* Update Immich to 1.62.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.62.1)
* Thumbhash on the web.
* Easier to host the Machine Learning container on a different machine.

[1.25.0]
* Update Immich to 1.63.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.63.0)
* feat(web): add zoom toggle icon by @mtaberna in #2873
* feat(web): Add album sorting to albums view by @klejejs in #2861
* feat(web): custom drop down button by @alextran1502 in #2887
* feat(web): add album to search result by @alextran1502 in #2900
* feat(web): Add keyboard event support to memory view by @klejejs in #2890
* fix(web): redirect to parent folder if asset viewer was open before reloading by @Sophtli in #2863
* fix(web): Mov files should show up in file picker. by @samip5 in #2886
* fix(web): keep video volume by @faupau03 in #2897
* fix(web): memory pause autoplay when scrolling down by @alextran1502 in #2923
* feat(server): Add support for many missing raw formats by @elliotclee in #2834
* feat(server): JPEG XL by @uhthomas in #2893
* feat(server): M2TS by @uhthomas in #2896
* feat(server): support for read-only assets and importing existing items in the filesystem by @alex-phillips in #2715
* fix(server): use HTTP status OK instead of CREATED for assets by @uhthomas in #2876
* fix(server): Fix person's assets retrieval order by @alextran1502 in #2920
* fix(server): transform isReadOnly DTO to boolean by @alextran1502 in #2912
* chore(server): remove openapi assertion for dart by @alextran1502 in #2916
* chore(server): Improve moveAsset log by @alextran1502 in #2878
* chore(server): patch dart openapi assertion by @alextran1502 in #2914
* chore(server): add Canon CR3 mime type by @416c616e in #2922
* refactor(server, web): create shared link by @jrasm91 in #2879
* Shared link with expiration creation time error.
* Support DNG file

[1.26.0]
* Update Immich to 1.64.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.64.0)
* Fixed issue with video progress bar interfering with action buttons and the seek mechanism - Thanks, @brighteyed, for the phenomenon fix for this.
* Guard against missing EXIF information on the Android version due to a lack of proper permission check
* Minor fixes for some raw file's acceptance criteria.
* Add more logs on mobile to help debugging some outstanding issues

[1.27.0]
* Update Immich to 1.65.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.65.0)
* feat(server): Add camera make and model to search criteria by @alextran1502 in #2982
* refactor(server): access permissions by @jrasm91 in #2910
* fix(server): add missing avi mime types and add tests by @uhthomas in #3001
* chore(server): check file extension for XMP instead of mimetype by @alex-phillips in #2990
* fix(server): empty tag responses should be considered valid by @mertalev in #2993
* chore(server): Image description disappears after toggle favorite by @KeszeiTheOne in #3009
* fix(server): live photos not playing in shared links/albums by @jrasm91 in #3008
* fix(server): use private cache by @uhthomas in #3017
* chore(server): add CLI tool to the server image by @ddshd in #2999
* chore(web): Only show Copy button in HTTPS context by @alextran1502 in #2983
* fix(web): Add m: to search query upon loading results. by @aero31aero in #2954
* fix(web): aspect ratio for photos with Rotate 270 CW orientation by @brighteyed in #3003
* fix(web): persist info by @martabal in #3013
* fix(web): aspect ratio for videos with 90 CW and 270 CW orientation by @brighteyed in #3023
* fix(web): Share link multi-select download icon showing when not available #3006 by @faupau03 in #3027
* Fix(web): drag n drop shared link by @faupau03 in #3030
* fix(ml): setting different clip by @mertalev in #2987
* fix(ml): clear model cache on load error by @mertalev in #2951

[1.28.0]
* Update Immich to 1.66.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.66.1)
* Choose a new feature photo for a person
* Shift-key selection on the web timeline
* fix(server): read file permission checks by @jrasm91 in #3046
* fix(server): fix more vector search results being returned than intended by @mertalev in #3042
* fix(server): h264 and hevc not respecting max bitrate by @mertalev in #3052
* fix(server): h264 videos failing to transcode in two-pass mode by @mertalev in #3053

[1.29.0]
* Update Immich to 1.67.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.67.1)
* Add the ability to merge similar faces
* Improve range selection behavior
* Reduce RAM usage of Machine Learning container
* Fix issues with the server cannot start

[1.29.1]
* Update Immich to 1.67.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.67.2)
* Send files with absolute urls cannot be read.

[1.30.0]
* Update Immich to 1.68.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.68.0)
* Add the ability to hide and show faces
* Better user interface to manage face’s name and facial recognition action
* Add shortcuts for common actions to photo viewer on the web

[1.31.0]
* Update Immich to 1.69.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.69.0)
* Add support for Motion photos on Android.
* Improve UI for hide and show face mechanism.
* Fix a bug where files with uppercase extension cannot be uploaded from the web.
* Fix a bug where faces after the merging process don’t get recognized in new assets.

[1.32.0]
* Update Immich to 1.70.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.70.0)
* The shortcut panel can be toggled using Shift + ? on the web
* Facial recognition job will now start from the most recent assets instead of the oldest assets, solving the issue of different babies getting recognized as the same person.

[1.33.0]
* Update Immich to 1.71.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.71.0)
* Add initial support for 360 degrees panoramas viewer on the web
* Merge faces suggestion based on name

[1.34.0]
* Update Immich to 1.72.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.72.0)
* Initial support for hardware transcoding.
* Manually upload assets from the mobile app.
* Optimized views on the web.
* Album description.

[1.34.1]
* Update Immich to 1.72.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.72.1)

[1.34.2]
* Update Immich to 1.72.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.72.2)
* Add `admin-cli` command to manage the instance via webterminal

[1.34.3]
* Update ffmpeg to version 6

[1.35.0]
* Update Immich to 1.73.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.73.0)
* Optimized viewing experience for the Album page.
* Added option to configure the generated thumbnail resolution.
* Added option to toggle visibility of the memory reel.
* Better quality for thumbnails and generated videos.

[1.36.0]
* Update Immich to 1.74.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.74.0)
* Optimized shared link view to use virtual viewports
* Add the ability to refresh metadata and regenerate thumbnails for individual photo or video.
* Upgrade to Flutter 3.13 and improvement of album UI/UX.
* Albums can now be viewed as a list on the web.
* Add the ability to set the birthday of a person.

[1.37.0]
* Update Immich to 1.75.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.75.0)
* Add the ability to use a configuration file for bootstrapping an Immich instance instead of using the administration web UI. Documentation
* Machine learning sub-jobs can now be configured via the administration web UI
* Slide show mode on the web
* And as always, bugs are fixed, and many other improvements also come with this release.

[1.37.1]
* Update Immich to 1.75.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.75.1)
* Fix issue with TypeSense doesn't get initialized in microservices. If you have uploaded new assets since v1.75.0, you can invoke the CLIP encoding for missing assets to get those indexes to the database.

[1.37.2]
* Update Immich to 1.75.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.75.2)
* Fix an issue with refreshing the job page; it doesn't render the correct button for each job.

[1.38.0]
* Update Immich to 1.76.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.76.0)
* Map view on mobile
* Customizable machine learning settings

[1.38.1]
* Update Immich to 1.76.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.76.1)

[1.38.2]
* Fix machine learning clip task

[1.39.0]
* Update Immich to 1.77.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.77.0)
* feat(web): face tooltips by @daniele-athome in #3924
* chore(web): create real anchors around admin sidebar buttons by @daniele-athome in #3925
* feat(web): Improved assets upload by @Willena in #3850
* feat(web): skeleton on asset loading by @JasBogans in #3867
* fix(web): images not loading on search and gallery by @valonsogit in #3902
* fix(web): sidebar artifact when toggle themes by @JasBogans in #3955
* fix(web): skeleton loading by @jrasm91 in #3972
* feat(web): show original uploader in shared album photo details by @mrijke in #3977
* chore(ml): memory optimisations by @mertalev in #3934
* dev(ml): fixed docker-compose.dev.yml, updated locust by @mertalev in #3951
* fix(ml): model downloading improvements by @mertalev in #3988
* refactor(web,server): use feature flags for oauth by @jrasm91 in #3928
* fix(server): non-nullable IsOptional by @mertalev in #3939
* test(server): server-info e2e tests by @jrasm91 in #3948
* feat(server): advanced settings for transcoding by @mertalev in #3775
* feat(server): wide gamut thumbnails by @mertalev in #3658
* fix(server): lint import order by @jrasm91 in #3974
* fix(server): await thumbnail generation before returning by @mertalev in #3975
* feat(server): consider shared albums in getByAssetId by @mrijke in #3978
* refactor(server): update asset endpoint by @jrasm91 in #3973

[1.39.1]
* Fix matplotlib cache directory
* Fix generic term search

[1.40.0]
* Update Immich to 1.78.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.78.0)
* Allow self-signed certificate on the mobile app
* Notable fix: Fix the issue with iOS background upload not working properly
* Notable fix: Better synchronization mechanism on the mobile app

[1.40.1]
* Update Immich to 1.78.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.78.1)
* Fixed issue using --import flag from the CLI doesn't work

[1.41.0]
* Update Immich to 1.79.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.79.0)
* Support for external libraries
* Set minimum detected face to show on your instance
* Notable fix: Video EXIF parsing
* Notable fix: Android background upload regression
* Notable fix: Jerky mobile's swipe behavior

[1.42.0]
* Update Immich to 1.80.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.80.0)
* The reverse geocoding settings have been moved to the admin settings pages. If you have set the level of accuracy in the '.env'in .env file, you will need to make the corresponding changes in the settings menu on the administration page on the web.
* Migrate thumbnails to a new folder structure by running the new migration job Admin > Settings > Migration. See [#4112](https://github.com/immich-app/immich/pull/4112) for more details)
* Added mechanism to store generated files for thumbnails and encoded-videos in subfolders.
* Added an ability to suggest people’s names during name assignment for faces.
* Added an option to show the sidebar menu for the people page in the user setting on the web.
* Added week number as an option for path templating.
* Bugfixes related to External Libraries, metadata processing, and Live Photos

[1.42.1]
* Update Immich to 1.81.0
* Update base image to 4.2.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.81.0)
* fix(server): delete face thumbnails when merging people by @danieldietzler in https://github.com/immich-app/immich/pull/4310
* feat(server): use host system timezone by @etnoy in https://github.com/immich-app/immich/pull/4313
* fix(server): incorrect video creation date EXIF extraction by @alextran1502 in https://github.com/immich-app/immich/pull/4309
* chore(server,web): bump node version to 20.8 by @etnoy in https://github.com/immich-app/immich/pull/4311
* fix(server): asset upload permissions for shared links by @danieldietzler in https://github.com/immich-app/immich/pull/4325
* fix(server): fallback to local timezone when rendering storage template by @jrasm91 in https://github.com/immich-app/immich/pull/4317
* chore(server): dev compose changes by @jrasm91 in https://github.com/immich-app/immich/pull/4316
* fix(server): library control doesn't apply to new library from the third row by @alextran1502 in https://github.com/immich-app/immich/pull/4331

[1.42.2]
* Update Immich to 1.81.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.81.1)
* feat(web, mobile): Options to show archived assets in map by @danieldietzler in #4293
* fix(server): Offset of random endpoint could be higher than user's asset count by @danieldietzler in #4342

[1.43.0]
* Update Immich to 1.82.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.82.0)
* The mobile app and server must be on the same version to work correctly.
* We identified two security flaws that have been fixed and, therefore, urge all Immich users to upgrade as soon as possible.
* Trash Feature
* Web Client WebSocket Feature
* Library Scanning Performance
* Time Bucket Grouping Accuracy
* Storage Template Improvements

[1.43.1]
* Update Immich to 1.82.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.82.1)
* Fixed Scrollbar offset from the web
* Optimized add/remove actions for albums
* Fixed WebSocket initial connection on the web

[1.44.0]
* ACTION REQUIRED: Re-run metadata extraction for videos impacted by the duration parsing bug (#4480)
* Update Immich to 1.83.0
* Update Typesense to 0.24.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.83.0)
* Asset stacking
* Shared links on mobile
* New storage template variables
* Custom theme
* Notable mention: PR #4474 has been merged, which is a prerequisite for additional facial recognition features, including unmerging and manually reassigning faces.

[1.45.0]
* Update Immich to 1.84.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.84.0)
* New mobile app bar and user profile screen
* Shared link with password option
* Shared album activity (web)
* Custom scan interval for external library
* Notable fix: Cannot download machine learning models from the S3 bucket

[1.46.0]
* Update Immich to 1.85.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.85.0)
* The server and mobile app must be on the same version for the application to work correctly.
* Global activity in a shared album and activity on the mobile app and album’s option
* Shuffle slideshow: New option to randomize the order
* Notable fix: Bulk data fetching for initial sync on the mobile app to fix the issue with a very large gallery, causing the app to crash due to an out-of-memory error.

[1.47.0]
* Update Immich to 1.86.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.86.0)
* Partner sharing on the main timeline (Web & Mobile)
* New map tile server
* Support for additional facial recognition models
* User’s avatar color
* Notable fix: “Missing” for the recognize faces job now will only process an asset a single time, even when no faces are detected (#4854)
* Notable fix: user first/last name has been merged into a single “name” field (#4915)

[1.48.0]
* Update Immich to 1.87.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.87.0)
* You can now use the mobile app without giving it full permission to access media storage, allowing you to access only remote assets that are backed up on the server.
* Machine learning models are now unloaded when unused, reducing the application's memory footprint.
* The /asset/import endpoint has been removed in favor of Libraries.
* Lower thumbnail resolution options

[1.49.0]
* Update Immich to 1.88.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.88.0)
* feat(server): bmp format by @jrasm91 in #5197
* chore(server): update new CLI into the image by @alextran1502 in #5192
* feat(web)!: SPA by @jrasm91 in #5069
* fix(web): improve year label position by @Funk66 in #5141
* fix(web): album sorting options by @martabal in #5127
* feat(web): new fonts by @alextran1502 in #5165
* chore(web): small font size improvement by @alextran1502 in #5190
* chore(web): remove deprecation message by @alextran1502 in #5115
* chore(web): album thumbnail size by @alextran1502 in #5196
* Fixed an issue with the video not being correctly placed on the timeline
* Fixed an issue with search on the web doesn't take into account the search phrase
* Fixed an issue of the year label overlapped on the timeline bar

[1.49.1]
* Update Immich to 1.88.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.88.2)
* Fixed issue with query unoptimized for the album and sharing page on the web. This fix is a revert of the fix for the web's album sorting. We will need to return to that issue and find an optimal way to perform the sort mechanism

[1.50.0]
* Update Immich to 1.89.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.89.0)
* Better reverse geocoding resource management (#5301)
* Search name when merging face
* Album sorting options (take 2)
* Set original datetime via sidecar in preparation for metadata writing (#5199, #5066)

[1.50.1]
* Update Immich to 1.89.0
* Requires Cloudron 7.6.3 for database restore to work
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.89.0)
* Better reverse geocoding resource management (#5301)
* Search name when merging face
* Album sorting options (take 2)
* Set original datetime via sidecar in preparation for metadata writing (#5199, #5066)

[1.51.0]
* Update Immich to 1.90.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.90.0)
* Edit metadata.
* Reassign person faces.
* Better handling of iCloud assets.
* Fixed bulk editing for asset's metadata on the web
* Fixed faces not update after reassign face in a video
* Fixed merge face panel only shows 10 people

[1.52.0]
* Update Immich to 1.91.4
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.91.0)
* Fixed cannot save exclusive patterns on the web
* Fixed cannot search for places
* Fixed smart search's job concurrency doesn't persist.
* Disable version check settings when config file is set
* Fixed migration issue when updated to 1.91
* Fixed inconsistent explore queries
* Fixed cannot open map cluster
* Fixed change people feature face doesn't registered on the web

[1.52.1]
* Update Immich to 1.92.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.92.0)
* Hardening storage template move operation: verify files are copied correctly when operating across filesystem boundaries
* External domain setting: allow the usage of a different domain for shared links
* Native hash calculation on iOS: significantly speeds up the initial hash calculation
* Introduce Onboarding flow for new instances: Show the admin common settings of the instance for a better user experience.
* CLI version 2.0.6 was released on npm: https://www.npmjs.com/package/@immich/cli
* Search is now working across the partner’s assets.

[1.52.2]
* Update Imich to 1.92.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.92.1)
* Not showing onboarding if the instance uses the config file

[1.53.0]
* Update Immich to 1.93.3
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.93.3)
* Improved face grouping for facial detection - switch to DBSCAN clustering algorithm.
* New usage quota mechanism for users - specify the maximum storage each user can use on the server.
* Improve the deletion actions on the mobile app - Present explicit options to clear up confusion about each action.
* Faster map rendering on the mobile app
* Force deletion with Shift + Del on the web app
* Notable fix: The web can now show assets with a date in the future.
* Fixed an issue with the toggle slider doesn't have any information/label on the web
* Fixed toggle button doesn't work in some settings forms
* Small fixes for the web

[1.53.1]
* Fix gunicorn usage

[1.54.0]
* Update Immich to 1.94.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.94.0)
* Automatically update the external libraries when the filesystem changes (experimental)
* Hardware acceleration for machine learning is now supported for ARM NN, CUDA, and OpenVINO.
* Search for people on the people page.
* Additional video transcoding options for audio and video codecs.
* New combobox component for selecting asset’s timezone on the web.
* Notable fix: Motion Photo on Samsung is now fixed.
* Notable fix: The blurry memory photo on the mobile app is now fixed.
* Fixed the issue that cannot log in with the OAuth button on the web

[1.54.1]
* Fix poetry install for machine-learning

[1.55.0]
* Update Immich to 1.97.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.97.0)
* Lots of major changes and update to latest pgvecto_rs postgres extension

[1.56.0]
* Update Immich to 1.98.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.98.0)
* Move library settings to the Administration page - Simplify the external library setup process.
* Context search with people filtering.
* File name and file extension search.
* Add an option to disable the required password change when creating a new user.
* Show people in the information sheet on the mobile app.
* A configurable timer is used to delete a user permanently.
* Fixed an issue with syncing the assets from the server on the mobile app
* Fixed archive assets are not properly allocated on the mobile app
* Fixed caching issue on the mobile app, causing jerkiness when scrolling on the timeline
* Fixed an issue with the library re-scanning assets if the library has more than 65,000 assets.

[1.57.0]
* Update Immich to 1.99.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.99.0)
* New logo
* [WEB] Sort photos and videos to display direction in an album.
* On-disk cache on the mobile app to provide a better viewing experience.
* There is a new endpoint for OpenTelemetry metrics to monitor performance.
* Optimized queries for large libraries.
* Add upload progress and speed indicator.
* XMP sidecar file is now recognized with both photo.ext.xmp and photo.xmp extensions.
* Drag to select - Now press your thumb to drag and select in the selection mode.
* Improve user feedback in the mobile app's backup and album selection screen.
* Various improvements for the library scanning process.

[1.58.0]
* Update Immich to 1.100.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.100.0)
* Ability to view different album ownership on the album page.
* Better user experience when creating a new album.
* Context menu items now come with associated icons.
* Slideshow control improvement.
* Added job metrics for Prometheus.
* Notable fix: Scroll stickiness on the mobile app.

[1.59.0]
* Update Immich to 1.101.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.101.0)
* fix(server): add missing file extensions to library files by @etnoy in https://github.com/immich-app/immich/pull/8342
* fix(server): map style not being available for shared assets by @danieldietzler in https://github.com/immich-app/immich/pull/8341
* refactor(server): move timeline operations to their own controller/service by @danieldietzler in https://github.com/immich-app/immich/pull/8325
* refactor(server): extract add/remove assets logic to utility function by @danieldietzler in https://github.com/immich-app/immich/pull/8329
* fix: sql generation issues by @jrasm91 in https://github.com/immich-app/immich/pull/8361
* fix(server): parameter for all places query by @mertalev in https://github.com/immich-app/immich/pull/8346
* feat(server): extensions for MPEG and 3GP by @mmomjian in https://github.com/immich-app/immich/pull/8400
* Fix external library path validation #8319 by @pablodre in https://github.com/immich-app/immich/pull/8366
* fix(server): penalize null geodata fields when searching places by @mertalev in https://github.com/immich-app/immich/pull/8408
* fix(server): sorting memory date by @ZlabiDev in https://github.com/immich-app/immich/pull/8432
* refactor(server): decouple generated images from image formats by @mertalev in https://github.com/immich-app/immich/pull/8246
* feat: persistent memories by @jrasm91 in https://github.com/immich-app/immich/pull/8330
* chore: update openapi by @alextran1502 in https://github.com/immich-app/immich/pull/8470

[1.60.0]
* Update Immich to 1.102.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.102.0)
* In-app language settings
* Haptic feedback control
* Enhanced UI/UX on the album list page
* AV1 transcoding
* Choose between WebP and JPEG for thumbnails and previews
* Option to use embedded preview in RAW images
* Option to fill the screen with slideshows view

[1.60.1]
* Update Immich to 1.102.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.102.2)
* Fixed Android mobile app randomly logout for user with background backup enabled
* Next attempt to fix auto-logout on Android
* Fixed issue with input focus reset on search people page

[1.60.2]
* Update Immich to 1.102.3
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.102.3)
* Actual fix for logout issue across the web and the mobile app

[1.61.0]
* Update Immich to 1.103.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.103.1)
* Fixed an issue of being unable to view detailed assets in partner sharing on the web.
* Read-only album sharing permission
* Permanent URL on the web for asset viewer
* Jump-to-date from memory view on the mobile app
* Action bar in memory view on the web
* Improve geocoding location data
* Notable fix: Occasional logout on iOS
* Notable fix: Asset's status sync issue on the mobile app
* Notable fix: Memory leak causing mobile app crashes when swiping continuously in the detail view

[1.61.1]
* Make config file writeable https://docs.cloudron.io/apps/immich/#settings

[1.62.0]
* Update Immich to 1.104.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.104.0)
* Editability of external library
* Notification foundation - SMTP Email

[1.63.0]
* Update Immich to 1.105.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.105.1)
* The mobile app now uses a more efficient sync algorithm.
* Fixed assets status not showing correctly when deleting assets from the server when opening the mobile app.
* Fixed a bug where deleting an external library would delete all local files

[1.64.0]
* Update Immich to 1.106.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.106.0)

[1.64.1]
* Update Immich to 1.106.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.106.2)

[1.64.2]
* Update Immich to 1.106.3
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.106.3)
* fix: postgres health check reporting any db without checksums as unhealthy by @mertalev in #10178
* fix: increase pixel limit for thumbnail generation by @mertalev in #10181
* fix: exiftool largefilesupport only set for the first call by @stephen304 in #10167
* fix: only run healthchecks when api worker is running on immich-server by @zackpollard in #10204
* fix: checkExistingAssets by @michelheusschen in #10192
* fix: no floats (replace with doubles) by @jrasm91 in #10218

[1.64.3]
* Update Immich to 1.106.4
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.106.4)

[1.65.0]
* Update Immich to 1.107.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.107.0)

[1.65.1]
* Update Immich to 1.107.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.107.2)

[1.66.0]
* Update Immich to 1.108.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.108.0)

[1.67.0]
* Update Immich to 1.109.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.109.2)

[1.68.0]
* Update Immich to 1.110.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.110.0)

[1.68.1]
* Update Immich to 1.111.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.111.0)
* Photo editor on the mobile app
* Notable fix: Random logout on the mobile app when the connection between the app and server is unstable.

[1.69.0]
* Update Immich to 1.112.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.112.0)
* Open in map view on the web
* Star rating
* Screen stays on and auto darkens when performing a long session of foreground upload
* Notable fix: Uploading assets from iCloud causing the iOS mobile app to crash
* Notable fix: Photos and videos belonging to a person are now all displayed in the person's view on the mobile app

[1.69.1]
* Update Immich to 1.112.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.112.1)
* Fixed entering backup screen always shows the permission prompt on Android
* Fixed the Load original image mechanism.

[1.70.0]
* Update Immich to 1.113.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.113.0)
* Folder view
* Tags
* Timeline improvements
* Library refresh stability
* Mobile album sync

[1.70.1]
* Update Immich to 1.113.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.113.1)
* This release fixes some bugs introduced in version v1.113.0 and added some enhancements to the new Folders and Tags feature

[1.71.0]
* Update Immich to 1.114.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.114.0)
* Tag improvements (including Lightroom support)
* Import faces from EXIF and XMP sidecars (Digikam format)
* Better handling of timezones
* Upload panel **New look**
* Automatic database reconnection

[1.72.0]
* Update Immich to 1.115.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.115.0)
* Administration button
* Manually link and unlink Live motion photos
* Default exclusion patterns
* Start-up folder checks
* Upload trash indicator

[1.72.1]
* Ensure geolocation database is part of the image

[1.73.0]
* Update Immich to 1.116.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.116.0)
* Improved.immich file boot checks
* Timezone improvements
* Tag clean up job
* Better person merge workflow
* Improve settings pages (web)
* Fixed possible startup failure due to .immich files
* Serve style.json directly from tiles.immich.app
* New "random" api for 3rd party apps
* Notable fix: Uncaught error causes the hashing process to abort entirely in the mobile app

[1.73.1]
* Update Immich to 1.116.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.116.2)

[1.74.0]
* Update Immich to 1.117.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.117.0)
* Better folder checks
* Download notifications (mobile)
* Support and feedback links (web)
* Upgrade history
* Asset thumbnail improvements

[1.74.0-1]
* Update Immich to 1.117.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.117.0)
* Better folder checks
* Download notifications (mobile)
* Support and feedback links (web)
* Upgrade history
* Asset thumbnail improvements

[1.75.0]
* Update Immich to 1.118.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.118.0)
* Mobile UI/UX improvement
* Option to refresh face detection
* Color filters for editing photos
* Timezone improvements
* Deprecated release notes section
* Better JPEG compression

[1.75.1]
* Update Immich to 1.118.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.118.1)
* fix(server): mobile oauth login by @​jrasm91 in https://github.com/immich-app/immich/pull/13474

[1.75.2]
* Update Immich to 1.118.2
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.118.2)
* fix: styling for last image in person grid by @MananJain-IITJ in #13444
* fix(server): ffmpeg matrices by @lyynd in #13461

[1.75.3]
* Update Immich to 1.119.0
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.119.0)
* Create new OAuth users without passwords
* Debounce email notifications for album changes
* Improve mobile app layout on tablets
* Notable fix: Face Detection using excessive RAM with OpenVINO
* Notable fix: transcodes not playing in the mobile app in certain situations

[1.75.4]
* Update Immich to 1.119.1
* [Full changelog](https://github.com/immich-app/immich/releases/tag/v1.119.1)
* feat(web): add job action to search page result by @alextran1502 in #13784

[1.76.0]
* Update Immich to 1.120.0
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.120.0)
* Built-in automatic database backups
* Timeline location indicator on scrolling
* The web is now on [Svelte 5](https://svelte.dev/blog/svelte-5-is-alive)
* Faster CPU transcoding for HDR videos
* Notable fix: Slow server start-up time on some systems
* feat(web): scrubber label and animation by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/13815
* feat: built-in automatic database backups by [@&#8203;zackpollard](https://github.com/zackpollard) in https://github.com/immich-app/immich/pull/13773
* feat: mount checks on a folder level by [@&#8203;zackpollard](https://github.com/zackpollard) in https://github.com/immich-app/immich/pull/13801
* feat(web): library settings crontab link includes existing expression by [@&#8203;zackpollard](https://github.com/zackpollard) in https://github.com/immich-app/immich/pull/13807
* feat(web): disable opening image and library sub-items by default by [@&#8203;ITestInProd](https://github.com/ITestInProd) in https://github.com/immich-app/immich/pull/13729
* fix(mobile): Reapply thumbnail image improvements and fix issue [#&#8203;13804](https://github.com/immich-app/immich/issues/13804) by [@&#8203;dvbthien](https://github.com/dvbthien) in https://github.com/immich-app/immich/pull/13835
* feat(server): use tonemapx for software tone-mapping by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/13785
* feat: render asset path by [@&#8203;cfitzw](https://github.com/cfitzw) in https://github.com/immich-app/immich/pull/13873
* chore(mobile): upgrade gradle by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/13901
* fix(server): handle N/A duration response from ffprobe by [@&#8203;jrasm91](https://github.com/jrasm91) in https://github.com/immich-app/immich/pull/13803
* fix: shutdown api process when another worker exits unexpectedly by [@&#8203;zackpollard](https://github.com/zackpollard) in https://github.com/immich-app/immich/pull/13802
* fix(mobile): Revert thumbnail image ui improvements  ([#&#8203;13655](https://github.com/immich-app/immich/issues/13655)) by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/13806
* fix(server): keep system config transformations by [@&#8203;jrasm91](https://github.com/jrasm91) in https://github.com/immich-app/immich/pull/13796
* fix(mobile): thumbnail not filled area on tablet by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/13808
* fix(mobile): search page by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/13833
* fix(mobile): not throwing error when cannot parse orientation value by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/13853
* fix(mobile): do not removed not backup asset when selecting the correspond options by [@&#8203;yashrajjain726](https://github.com/yashrajjain726) in https://github.com/immich-app/immich/pull/13256
* fix(server): wrong image dimensions for RAW files (RAF, CR2) (also fixes face preview) by [@&#8203;C-Otto](https://github.com/C-Otto) in https://github.com/immich-app/immich/pull/13377
* fix: healthcheck if custom host is set by [@&#8203;danieldietzler](https://github.com/danieldietzler) in https://github.com/immich-app/immich/pull/13887
* fix: remove duplicateIds on unique assets by [@&#8203;Pranay-Pandey](https://github.com/Pranay-Pandey) in https://github.com/immich-app/immich/pull/13752
* chore(docs): update german readme to latest version by [@&#8203;MickLesk](https://github.com/MickLesk) in https://github.com/immich-app/immich/pull/13824
* chore(docs): Add Immich Public Proxy to the Community Projects list by [@&#8203;alangrainger](https://github.com/alangrainger) in https://github.com/immich-app/immich/pull/13836
* docs: fail2ban community project by [@&#8203;mmomjian](https://github.com/mmomjian) in https://github.com/immich-app/immich/pull/13943
* chore(web): update translations by [@&#8203;weblate](https://github.com/weblate) in https://github.com/immich-app/immich/pull/13688
* chore(web): update translations by [@&#8203;weblate](https://github.com/weblate) in https://github.com/immich-app/immich/pull/13810
* [@&#8203;ITestInProd](https://github.com/ITestInProd) made their first contribution in https://github.com/immich-app/immich/pull/13729
* [@&#8203;alangrainger](https://github.com/alangrainger) made their first contribution in https://github.com/immich-app/immich/pull/13836

[1.76.1]
* Update Immich to 1.120.1
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.120.1)
* fix(server): cannot render email template by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/13957
* fix(server): allow starting backup through API and fix pg_dumpall args when using database URLs by [@&#8203;dotlambda](https://github.com/dotlambda) in https://github.com/immich-app/immich/pull/13970
* fix(server): database backups compatible with deduplication by [@&#8203;Scrumplex](https://github.com/Scrumplex) in https://github.com/immich-app/immich/pull/13965
* fix(mobile): video player not playing in full size on Android by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/13986
* fix: docker link by [@&#8203;danieldietzler](https://github.com/danieldietzler) in https://github.com/immich-app/immich/pull/13956
* docs: improve custom-locations wording to be easier to read by [@&#8203;slamp](https://github.com/slamp) in https://github.com/immich-app/immich/pull/13849
* docs: Added a note about avoiding redundant database backups by [@&#8203;thariq-shanavas](https://github.com/thariq-shanavas) in https://github.com/immich-app/immich/pull/13958
* chore: tidy up backup-and-restore.md by [@&#8203;bo0tzz](https://github.com/bo0tzz) in https://github.com/immich-app/immich/pull/13961
* docs: 50k stars by [@&#8203;danieldietzler](https://github.com/danieldietzler) in https://github.com/immich-app/immich/pull/13964
* docs: add backups to startup folders list by [@&#8203;yodatak](https://github.com/yodatak) in https://github.com/immich-app/immich/pull/13967
* docs: update roadmap by [@&#8203;jrasm91](https://github.com/jrasm91) in https://github.com/immich-app/immich/pull/13984
* [@&#8203;slamp](https://github.com/slamp) made their first contribution in https://github.com/immich-app/immich/pull/13849
* [@&#8203;yodatak](https://github.com/yodatak) made their first contribution in https://github.com/immich-app/immich/pull/13967
* [@&#8203;Scrumplex](https://github.com/Scrumplex) made their first contribution in https://github.com/immich-app/immich/pull/13965

[1.76.2]
* Update immich to 1.120.2
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.120.2)
* Config updates were not correctly applied to some components.
* The mobile app doesnt initialize the date locale for some languages.
* feat(web): stable json settings export by [@&#8203;mcarbonne](https://github.com/mcarbonne) in https://github.com/immich-app/immich/pull/14036
* feat(server): use pg_dumpall version that matches the database version by [@&#8203;zackpollard](https://github.com/zackpollard) in https://github.com/immich-app/immich/pull/14083
* fix(server): support non-default Postgres port when taking a backup by [@&#8203;jrasm91](https://github.com/jrasm91) in https://github.com/immich-app/immich/pull/13992
* fix(server): thumbnail rotation when using embedded previews by [@&#8203;zhaoterryy](https://github.com/zhaoterryy) in https://github.com/immich-app/immich/pull/13948
* fix(web): use locale for scrubber label when scrolling by [@&#8203;michelheusschen](https://github.com/michelheusschen) in https://github.com/immich-app/immich/pull/14012
* fix(mobile): make sure date locale is initialized for some languages by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/14035
* fix(server): attempt to delete failed backups immediately after failure by [@&#8203;zackpollard](https://github.com/zackpollard) in https://github.com/immich-app/immich/pull/13995
* fix: config updates not applying for job and storage template service by [@&#8203;zackpollard](https://github.com/zackpollard) in https://github.com/immich-app/immich/pull/14074

[1.77.0]
* Update immich to 1.121.0
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.121.0)
* Fallback to system fonts for Cyrillic letters on the mobile app
* Multiselect using the shift key in the search result view
* **Notable fixes**: album sync on the mobile app always ran when reopening the app, leading to degradation in performance and browsing
* feat: add minimal devcontainer setup by [@&#8203;mcarbonne](https://github.com/mcarbonne) in [#&#8203;14038](https://github.com/immich-app/immich/pull/14038)
* refactor(mobile): video controls by [@&#8203;mertalev](https://github.com/mertalev) in [#&#8203;14086](https://github.com/immich-app/immich/pull/14086)
* feat: use dateTimeOriginal to calculate album date by [@&#8203;p2kmgcl](https://github.com/p2kmgcl) in [#&#8203;14119](https://github.com/immich-app/immich/pull/14119)
* feat(web): Added tag button to the context menu in the favorites page by [@&#8203;IMBeniamin](https://github.com/IMBeniamin) in [#&#8203;14156](https://github.com/immich-app/immich/pull/14156)
* feat: adding photo & video storage space to server stats by [@&#8203;weathondev](https://github.com/weathondev) in [#&#8203;14125](https://github.com/immich-app/immich/pull/14125)
* feat(web): Implement keep this delete others for asset stacks  by [@&#8203;bdavis2-PCTY](https://github.com/bdavis2-PCTY) in [#&#8203;14217](https://github.com/immich-app/immich/pull/14217)
* feat: Added shortcuts, shift-multi select, and missing menu options to Search (Galleryviewer) by [@&#8203;weathondev](https://github.com/weathondev) in [#&#8203;14213](https://github.com/immich-app/immich/pull/14213)
* feat(server): faster geodata import by [@&#8203;mertalev](https://github.com/mertalev) in [#&#8203;14241](https://github.com/immich-app/immich/pull/14241)
* fix(server): Some MTS videos fail to generate thumbnail by [@&#8203;Lukasdotcom](https://github.com/Lukasdotcom) in [#&#8203;14134](https://github.com/immich-app/immich/pull/14134)
* fix(web): textarea autogrow height by [@&#8203;duckimann](https://github.com/duckimann) in [#&#8203;13983](https://github.com/immich-app/immich/pull/13983)
* fix: Routing back button in sharedLinks page by [@&#8203;Pranay-Pandey](https://github.com/Pranay-Pandey) in [#&#8203;13703](https://github.com/immich-app/immich/pull/13703)
* fix(web): ensure current asset index stays within bounds by [@&#8203;michelheusschen](https://github.com/michelheusschen) in [#&#8203;14013](https://github.com/immich-app/immich/pull/14013)
* fix(web): saving pasted coordinates by [@&#8203;michelheusschen](https://github.com/michelheusschen) in [#&#8203;14143](https://github.com/immich-app/immich/pull/14143)
* fix(web): update description height when navigating between assets by [@&#8203;michelheusschen](https://github.com/michelheusschen) in [#&#8203;14145](https://github.com/immich-app/immich/pull/14145)
* fix(web): allow selecting people after clearing search options by [@&#8203;michelheusschen](https://github.com/michelheusschen) in [#&#8203;14146](https://github.com/immich-app/immich/pull/14146)
* fix(web): prevent infinite loop when modifying stacked asset by [@&#8203;michelheusschen](https://github.com/michelheusschen) in [#&#8203;14162](https://github.com/immich-app/immich/pull/14162)
* fix(cli): Concurrency not fully using queue potential by [@&#8203;Tiefseetauchner](https://github.com/Tiefseetauchner) in [#&#8203;11828](https://github.com/immich-app/immich/pull/11828)
* fix(web): don't refresh the panorama viewer when modifying asset by [@&#8203;michelheusschen](https://github.com/michelheusschen) in [#&#8203;14163](https://github.com/immich-app/immich/pull/14163)
* fix: show tags when viewing stacked assets by [@&#8203;michelheusschen](https://github.com/michelheusschen) in [#&#8203;14199](https://github.com/immich-app/immich/pull/14199)
* fix(web): layout shifting when scrolling up by [@&#8203;alextran1502](https://github.com/alextran1502) in [#&#8203;14226](https://github.com/immich-app/immich/pull/14226)
* fix(server): remove unnecessary guc settings for vector search by [@&#8203;mertalev](https://github.com/mertalev) in [#&#8203;14237](https://github.com/immich-app/immich/pull/14237)
* fix: parse quota claim as a number by [@&#8203;danieldietzler](https://github.com/danieldietzler) in [#&#8203;14178](https://github.com/immich-app/immich/pull/14178)

[1.78.0]
* Update immich to 1.122.0
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.122.0)
* HDR video support in the mobile app
* Multiple URLs for machine learning service
* Automatic switching between server URLs in the mobile app
* Ability to hide users when searching in an Immich instance
* Access the most recent albums through the web navigation bar
* Custom email templates
* Automatically clean up files left behind by interrupted uploads
* More responsive hosted maps for users in Oceania
* Notable fix: swiping between videos failing on older Android devices

[1.78.1]
* Update immich to 1.122.1
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.122.1)
* fix(web): misaligned icon on Firefox by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/14500
* fix(server): images with non-ascii names failing to load by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/14512

[1.78.2]
* Update immich to 1.122.2
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.122.2)
* fix(web): recent albums sort by [@&#8203;michelheusschen](https://github.com/michelheusschen) in https://github.com/immich-app/immich/pull/14545
* fix(mobile): fix translations on search page by [@&#8203;Cotterman-b](https://github.com/Cotterman-b) in https://github.com/immich-app/immich/pull/14533
* chore(mobile): disable Impeller by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/14589
* [@&#8203;Cotterman-b](https://github.com/Cotterman-b) made their first contribution in https://github.com/immich-app/immich/pull/14533

[1.78.3]
* checklist added to CloudronManifest
* sendmail supports displayname
* COUDRON_OIDC_PROVIDER_NAME implemented

[1.78.4]
* Update immich to 1.122.3
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.122.3)
* feat: Add support for vob by [@&#8203;Lukasdotcom](https://github.com/Lukasdotcom) in https://github.com/immich-app/immich/pull/14590
* feat(web): allow tags to be applied in bulk on search, personID, and memory-viewer pages by [@&#8203;Menghini](https://github.com/Menghini) in https://github.com/immich-app/immich/pull/14368
* fix(server): partial fallback for hardware transcoding by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/14611
* fix(mobile): not being able to zoom into live photos by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/14608
* chore(docs): stronger discouraging of non-Linux installations by [@&#8203;mmomjian](https://github.com/mmomjian) in https://github.com/immich-app/immich/pull/14620
* chore(docs): add Kodi plugin for Immich to the Community Projects list by [@&#8203;vladd11](https://github.com/vladd11) in https://github.com/immich-app/immich/pull/14586
* [@&#8203;vladd11](https://github.com/vladd11) made their first contribution in https://github.com/immich-app/immich/pull/14586
* [@&#8203;Menghini](https://github.com/Menghini) made their first contribution in https://github.com/immich-app/immich/pull/14368

[1.79.0]
* Update immich to 1.123.0
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.123.0)
* feat(server): Merge Faces sorted by Similarity by [@&#8203;Lukasdotcom](https://github.com/Lukasdotcom) in https://github.com/immich-app/immich/pull/14635
* feat(album): album view sort order by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/14648
* feat(server): add Bash healthcheck script by [@&#8203;000yesnt](https://github.com/000yesnt) in https://github.com/immich-app/immich/pull/14704
* fix(web): live photo link action by [@&#8203;michelheusschen](https://github.com/michelheusschen) in https://github.com/immich-app/immich/pull/14668
* fix(web): allow minimizing upload panel by [@&#8203;michelheusschen](https://github.com/michelheusschen) in https://github.com/immich-app/immich/pull/14663
* fix(web): upload info panel covers timeline navigation bar by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/14651
* fix(server): fixed email footer image stretched [#&#8203;14617](https://github.com/immich-app/immich/issues/14617) by [@&#8203;TimVanOnckelen](https://github.com/TimVanOnckelen) in https://github.com/immich-app/immich/pull/14671

[1.80.0]
* Update immich to 1.124.0
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.124.0)
* Create tags on the fly.
* Asset actions on the folder view
* Documentation updates
* Notable fix: a bug in the loading of the reverse geocoding data
* Notable fix: do not try to delete offline assets when they expire from the trash
* feat(web): Add "set as featured" option for an asset by [@&#8203;IMBeniamin](https://github.com/IMBeniamin) in https://github.com/immich-app/immich/pull/14879
* feat(web): create tag on the fly by [@&#8203;MehringTing](https://github.com/MehringTing) in https://github.com/immich-app/immich/pull/14726

[1.80.1]
* Update immich to 1.124.2
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.124.2)
* Fixed a bug where various jobs don't run on new external library assets.
* Fixed a bug where multi-selection in the folder view is not cleared when changing folders
* fix(server): generate thumbnails for external assets by [@&#8203;etnoy](https://github.com/etnoy) in https://github.com/immich-app/immich/pull/15183

[1.81.0]
* Update immich to 1.125.1
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.125.1)
* Node.js security update
* Share-to mechanism on the mobile app
* Enable slideshow everywhere on the web app.
* Fixed a bug where the Android app cannot get newly taken photos from the gallery. However, this fix requires us to roll back another mechanism that allows the app to choose empty albums as targets for backup.
* Fixed a bug where playing particular video codecs would crash the Android mobile app.
* chore(server)!: default max bitrate unit to kbps by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15264

[1.81.1]
* Update immich to 1.125.3
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.125.3)
* Fixed an issue where changing the machine learning model doesn't change the vector size requirement
* Fixed an issue where the bulk location edit triggered an error
* Fixed an issue where the album cannot be loaded in the mobile app if any of the assets in the album are in the trash
* Fixed an issue where searchRandom endpoint doesn't return randomize items
* Fixed some issues regarding the timezone topic
* chore(server): print stack in case of worker error by [@&#8203;etnoy](https://github.com/etnoy) in https://github.com/immich-app/immich/pull/15632
* fix: increase upload timeout by [@&#8203;jdicioccio](https://github.com/jdicioccio) in https://github.com/immich-app/immich/pull/15588
* fix(mobile): improve timezone picker by [@&#8203;gaganyadav80](https://github.com/gaganyadav80) in https://github.com/immich-app/immich/pull/15615
* fix(server): changing vector dim size by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15630
* fix(server): bulk update location by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15642
* fix(server): do not reset fileCreatedDate by [@&#8203;C-Otto](https://github.com/C-Otto) in https://github.com/immich-app/immich/pull/15650
* fix(server): do not count deleted assets for album summary by [@&#8203;C-Otto](https://github.com/C-Otto) in https://github.com/immich-app/immich/pull/15668
* fix(server): avoid duplicate rows in album queries by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15670
* fix(web): neon overflow on mobile screen by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15676
* fix(server): /search/random API returns same assets every call by [@&#8203;sudbrack](https://github.com/sudbrack) in https://github.com/immich-app/immich/pull/15682
* [@&#8203;jdicioccio](https://github.com/jdicioccio) made their first contribution in https://github.com/immich-app/immich/pull/15588
* [@&#8203;idkwhyiusethisname](https://github.com/idkwhyiusethisname) made their first contribution in https://github.com/immich-app/immich/pull/15637
* [@&#8203;gaganyadav80](https://github.com/gaganyadav80) made their first contribution in https://github.com/immich-app/immich/pull/15615
* [@&#8203;ferraridamiano](https://github.com/ferraridamiano) made their first contribution in https://github.com/immich-app/immich/pull/15683
* [@&#8203;sudbrack](https://github.com/sudbrack) made their first contribution in https://github.com/immich-app/immich/pull/15682
* Fixed a bug where the timeline shows a placeholder in some sections
* Fixed a bug where using server URL with `sslmode` doesn't get parsed correctly
* Fixed a bug where LivePhotos doesn't generate thumbnails
* Fixed a bug where no EXIF data is returned for the deduplication view
* Fixed a bug where albums with archived assets don't show up on the mobile app
* Fixed a bug where hard refresh only refreshed assets and not albums
* Fixed a bug where the `updatedAt` column doesn't update on some assets and album actions
* Fixed a bug where `searchRandom` doesn't return the correct data format
* feat(web): neon light behinds auth forms by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15570
* fix(mobile): translation (no /api, experimental features) by [@&#8203;mmomjian](https://github.com/mmomjian) in https://github.com/immich-app/immich/pull/15600
* fix(server): `getTimeBuckets` not handling boolean filters correctly by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15567
* fix(web): auth page padding by [@&#8203;jrasm91](https://github.com/jrasm91) in https://github.com/immich-app/immich/pull/15569
* fix(server): set `updatedAt` on updates by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15573
* fix(server): Fix for sorting faces during merging by [@&#8203;Lukasdotcom](https://github.com/Lukasdotcom) in https://github.com/immich-app/immich/pull/15571
* fix(server): `searchRandom` response by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15580
* fix: login page by [@&#8203;jrasm91](https://github.com/jrasm91) in https://github.com/immich-app/immich/pull/15613
* fix(mobile): full refresh doesn't get albums by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15560
* fix(server): link live photos by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15612
* fix: demo login page by [@&#8203;jrasm91](https://github.com/jrasm91) in https://github.com/immich-app/immich/pull/15616
* fix(server): no exif metadata in the deduplication utility by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15585
* fix(mobile): deletion of single assets by [@&#8203;Saschl](https://github.com/Saschl) in https://github.com/immich-app/immich/pull/15597
* fix(server): failed to get albums with archived assets by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15611
* fix(server): migration mentions public schema by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15622
* fix(mobile): failed to load ga/gl locale by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15623
* fix(server): `sslmode` not working by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15587

[1.81.2]
* Update immich to 1.125.6
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.125.6)
* Fixed a bug where thumbnail generation job queue all person faces at midnight
* fix(server): person thumbnail generation always being queued by [@&#8203;mertalev](https://github.com/mertalev) in https://github.com/immich-app/immich/pull/15734
* Fixed a bug where the album page cannot be accessed if any album with its assets is in the trash.
* Fixed a bug where deduplication detection doesn't return any result
* Fixed a bug where the date picker component caused a rendering error if the app language was not in English
* fix(web): sort folders by [@&#8203;C-Otto](https://github.com/C-Otto) in https://github.com/immich-app/immich/pull/15691
* fix(server): cannot render album page when all assets of an album are in trash by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15690
* fix(server): duplicate detection by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15727
* fix(mobile): locale option causes the datetime filter error out by [@&#8203;alextran1502](https://github.com/alextran1502) in https://github.com/immich-app/immich/pull/15704

[1.81.3]
* Update immich to 1.125.7
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.125.7)
* fix(server): Allow negative rating (for rejected images) by [@&#8203;chkuendig](https://github.com/chkuendig) in https://github.com/immich-app/immich/pull/15699
* feat: resolution selection and default preview playback for 360 panorama videos by [@&#8203;pastleo](https://github.com/pastleo) in https://github.com/immich-app/immich/pull/15747
* feat: add support for JPEG 2000 by [@&#8203;ayykamp](https://github.com/ayykamp) in https://github.com/immich-app/immich/pull/15710
* fix(server): Update vaapi-wsl to include dxg by [@&#8203;Mraedis](https://github.com/Mraedis) in https://github.com/immich-app/immich/pull/15759
* fix(web): do not throw error when hash fails by [@&#8203;RiggiG](https://github.com/RiggiG) in https://github.com/immich-app/immich/pull/15740
* fix(web): cancel people merge selection: do not show "Change name successfully" notification by [@&#8203;afv](https://github.com/afv) in https://github.com/immich-app/immich/pull/15744
* fix: show local dates for range in album summary by [@&#8203;C-Otto](https://github.com/C-Otto) in https://github.com/immich-app/immich/pull/15654
* fix(server): restore user by [@&#8203;jrasm91](https://github.com/jrasm91) in https://github.com/immich-app/immich/pull/15763
* fix(web): update recent album after edit by [@&#8203;antoniosarro](https://github.com/antoniosarro) in https://github.com/immich-app/immich/pull/15762

[1.82.0]
* Update immich to 1.126.1
* [Full Changelog](https://github.com/immich-app/immich/releases/tag/v1.126.1)
* Fixes a compatibility issue with the mobile app
* [FOSDEM talk](https://video.fosdem.org/2025/h2215/fosdem-2025-5052-immich-self-hosted-photo-and-video-management-solution.mp4)
* Search improvement on assets description and tags
* Revamp places UI on the web
* Mark people as favorite
* Shared link UI improvement
* Cursed knowledge candidate: fixes a bug where the database timezone causes the timezone to be interpreted as an invalid date in Javascript.
* feat: add searching by tags by [@&#8203;dav-wolff](https://github.com/dav-wolff) in [#&#8203;15395](https://github.com/immich-app/immich/pull/15395)
* feat(web): revamp places by [@&#8203;kvalev](https://github.com/kvalev) in [#&#8203;12219](https://github.com/immich-app/immich/pull/12219)
* feat(server): synology exclusion patterns by [@&#8203;etnoy](https://github.com/etnoy) in [#&#8203;15773](https://github.com/immich-app/immich/pull/15773)
* feat(mobile): Add filter to people_picker.dart by [@&#8203;jforseth210](https://github.com/jforseth210) in [#&#8203;15771](https://github.com/immich-app/immich/pull/15771)
* feat: search by description by [@&#8203;alextran1502](https://github.com/alextran1502) in [#&#8203;15818](https://github.com/immich-app/immich/pull/15818)
* feat(mobile): Use `NavigationRail` when the screen is in landscape mode by [@&#8203;ferraridamiano](https://github.com/ferraridamiano) in [#&#8203;15885](https://github.com/immich-app/immich/pull/15885)
* feat(web): merge suggestion modal: focus on Yes button by default. by [@&#8203;afv](https://github.com/afv) in [#&#8203;15827](https://github.com/immich-app/immich/pull/15827)
* fix(mobile): improved the visibility of backup cloud icon on lighter images by [@&#8203;meesam4687](https://github.com/meesam4687) in [#&#8203;15886](https://github.com/immich-app/immich/pull/15886)
* feat: Mark people as favorite by [@&#8203;arnolicious](https://github.com/arnolicious) in [#&#8203;14866](https://github.com/immich-app/immich/pull/14866)
* feat(api): set person color by [@&#8203;jrasm91](https://github.com/jrasm91) in [#&#8203;15937](https://github.com/immich-app/immich/pull/15937)
* feat(web): shared link filters by [@&#8203;jrasm91](https://github.com/jrasm91) in [#&#8203;15948](https://github.com/immich-app/immich/pull/15948)
* feat: view album shared links by [@&#8203;jrasm91](https://github.com/jrasm91) in [#&#8203;15943](https://github.com/immich-app/immich/pull/15943)
* fix(web): shared link date range by [@&#8203;jrasm91](https://github.com/jrasm91) in [#&#8203;15802](https://github.com/immich-app/immich/pull/15802)
* feat(web): Updated Onboarding page by [@&#8203;OkayStark](https://github.com/OkayStark) in [#&#8203;15880](https://github.com/immich-app/immich/pull/15880)
* fix(server): memory lane assets order by [@&#8203;alextran1502](https://github.com/alextran1502) in [#&#8203;15882](https://github.com/immich-app/immich/pull/15882)
* fix(server): queue missing metadata by [@&#8203;etnoy](https://github.com/etnoy) in [#&#8203;15864](https://github.com/immich-app/immich/pull/15864)
* fix(server): for individual shares not showing thumbnails by [@&#8203;Lukasdotcom](https://github.com/Lukasdotcom) in [#&#8203;15895](https://github.com/immich-app/immich/pull/15895)
* fix(web): prevent accidental modal closures on mouseup outside by [@&#8203;afv](https://github.com/afv) in [#&#8203;15900](https://github.com/immich-app/immich/pull/15900)
* fix: call hexOrBufferToBase64 for stripMetadata thumbhash by [@&#8203;bo0tzz](https://github.com/bo0tzz) in [#&#8203;15917](https://github.com/immich-app/immich/pull/15917)
* fix(server): always get UTC dates from postgres by [@&#8203;jrasm91](https://github.com/jrasm91) in [#&#8203;15920](https://github.com/immich-app/immich/pull/15920)
* fix(server): validate oauth profile has a sub by [@&#8203;jrasm91](https://github.com/jrasm91) in [#&#8203;15967](https://github.com/immich-app/immich/pull/15967)

